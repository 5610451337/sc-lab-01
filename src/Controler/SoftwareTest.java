package Controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import model.CashCard;
import gui.SoftwareFrame;

public class SoftwareTest {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SoftwareTest();
	}

	public SoftwareTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		/*
		 * Student may modify this code and write your result here
		 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
		 */
		CashCard C1 = new CashCard();
		C1.check();
		C1.deposit(100);
		C1.check();
		C1.withdraw(50);
		C1.check();
		frame.setResult(C1.toString());
		

	}

	ActionListener list;
	SoftwareFrame frame;
}
